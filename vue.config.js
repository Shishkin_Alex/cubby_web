module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
        @import "~@/_variables.sass"
        @import "~@/_fonts.sass"
        `,
      },
    },
  },
};
