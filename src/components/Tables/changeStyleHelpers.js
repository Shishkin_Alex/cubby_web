/* eslint-disable */

export default {
  forEachCell(selected, func) {
    // get cells range

    const { from, to } = selected;

    // fix from top left to bottom right cell
    if (from.row > to.row) [from.row, to.row] = [to.row, from.row];
    if (from.col > to.col) [from.col, to.col] = [to.col, from.col];

    for (let i = from.row; i <= to.row; i++) {
      for (let j = from.col; j <= to.col; j++) {
        func(i, j, from, to);
      }
    }
  },
  forEachCol(selected, func) {
    // get cells range
    const range = selected;
    const { from, to } = range;

    // fix from top left to bottom right cell
    if (from.row > to.row) [from.row, to.row] = [to.row, from.row];
    if (from.col > to.col) [from.col, to.col] = [to.col, from.col];

    for (let j = from.col; j <= to.col; j++) {
      func(j, from, to);
    }
  },
  forEachRow(selected, func) {
    // get cells range
    const range = selected;
    const { from, to } = range;

    // fix from top left to bottom right cell
    if (from.row > to.row) [from.row, to.row] = [to.row, from.row];
    if (from.col > to.col) [from.col, to.col] = [to.col, from.col];

    for (let i = from.row; i <= to.row; i++) {
      func(i, from, to);
    }
  },
  bold(cont, key, val) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};
      if (styles[i][j].fontWeight == 'bold') styles[i][j].fontWeight = null
      else styles[i][j].fontWeight = 'bold';
    });
  },
  borderAll(cont) {

    const styles = cont.styles;
    this.forEachCell(cont.selectedTable, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};

      if (j == from.col && j - 1 != -1) {
        if (!styles[i][j - 1]) styles[i][j - 1] = {};
        styles[i][j - 1].borderRight = '1px solid #000';
      }

      if (i == from.row && i - 1 != -1) {
        if (!styles[i - 1]) styles[i - 1] = {};
        if (!styles[i - 1][j]) styles[i - 1][j] = {};
        styles[i - 1][j].borderBottom = '1px solid #000';
      }

      styles[i][j].borderBottom = '1px solid #000';
      styles[i][j].borderRight = '1px solid #000';
    });
  },
  borderOutline(cont) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      switch (i) {
        case from.row:
          if (!styles[from.row - 1]) styles[from.row - 1] = {};
          if (!styles[from.row - 1][j]) styles[from.row - 1][j] = {};
          styles[from.row - 1][j].borderBottom = '1px solid #000';
          break;
        case to.row:
          if (!styles[to.row]) styles[to.row] = {};
          if (!styles[to.row][j]) styles[to.row][j] = {};
          styles[to.row][j].borderBottom = '1px solid #000';
          break;
      }
      switch (j) {
        case from.col:
          if (!styles[i]) styles[i] = {};
          if (!styles[i][from.col - 1]) styles[i][from.col - 1] = {};
          styles[i][from.col - 1].borderRight = '1px solid #000';
          break;
        case to.col:
          if (!styles[i]) styles[i] = {};
          if (!styles[i][to.col]) styles[i][to.col] = {};
          styles[i][to.col].borderRight = '1px solid #000';
          break;
      }
    });
  },
  borderBottom(cont) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCol(tableRef, (j, from, to) => {
      if (!styles[to.row]) styles[to.row] = {};
      if (!styles[to.row][j]) styles[to.row][j] = {};
      styles[to.row][j].borderBottom = '1px solid #000';
    });
  },
  borderTop(cont) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCol(tableRef, (j, from, to) => {
      if (!styles[from.row - 1]) styles[from.row - 1] = {};
      if (!styles[from.row - 1][j]) styles[from.row - 1][j] = {};
      styles[from.row - 1][j].borderBottom = '1px solid #000';
    });
  },
  borderLeft(cont) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachRow(tableRef, (i, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][from.col - 1]) styles[i][from.col - 1] = {};
      styles[i][from.col - 1].borderRight = '1px solid #000';
    });
  },
  borderRight(cont) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachRow(tableRef, (i, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][to.col]) styles[i][to.col] = {};
      styles[i][to.col].borderRight = '1px solid #000';
    });
  },
  clearBorder(cont) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (styles[i][j]) {
        if (j == from.col && j - 1 != -1) styles[i][j - 1].borderRight = null;
        if (i == from.row && i - 1 != -1) styles[i - 1][j].borderBottom = null;
        styles[i][j].borderBottom = null;
        styles[i][j].borderRight = null;
      }
    });
  },
  textAlign(cont, val) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};
      styles[i][j].textAlign = val;
    });
  },
  textColor(cont, color) {
    const tableRef = cont.selectedTable
    const styles = Object.assign({},cont.styles);

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};
      cont.$delete(cont.styles[i][j], color)
      cont.$set(cont.styles[i][j], 'color', color.hex)
    });
  },
  textSize(cont, size) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};
      cont.$delete(cont.styles[i][j], 'fontSize')
      cont.$set(cont.styles[i][j], 'fontSize', `${size}px`)
    });
  },
  textFont(cont, font) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};
      styles[i][j].fontFamily = font;
    });
  },
  cellBackground(cont, color) {
    const tableRef = cont.selectedTable
    const styles = cont.styles;

    this.forEachCell(tableRef, (i, j, from, to) => {
      if (!styles[i]) styles[i] = {};
      if (!styles[i][j]) styles[i][j] = {};
      styles[i][j].backgroundColor = color.hex;
    });
  },

};
