export default {
  particles: {
    number: {
      value: 8,
      density: {
        enable: true,
        value_area: 1024,
      },
    },
    color: {
      value: '#fff',
    },
    shape: {
      type: 'image',
      stroke: {
        width: 0,
        color: '#000',
      },
      polygon: {
        nb_sides: 8,
      },
      image: {
        src: 'logo_big.png',
        width: 40,
        height: 48,
      },
    },
    opacity: {
      value: 16,
      random: true,
      anim: {
        enable: false,
        speed: 16,
        opacity_min: 0.25,
        sync: true,
      },
    },
    size: {
      value: 128,
      random: true,
      anim: {
        enable: true,
        speed: 8,
        size_min: 32,
        sync: true,
      },
    },
    line_linked: {
      enable: false,
      distance: 128,
      color: '#fff',
      opacity: 0.5,
      width: 1,
    },
    move: {
      enable: true,
      speed: 4,
      direction: 'none',
      random: true,
      straight: false,
      out_mode: 'bounce',
      bounce: false,
      attract: {
        enable: true,
        rotateX: 512,
        rotateY: 1024,
      },
    },
  },
  interactivity: {
    detect_on: 'canvas',
    events: {
      onhover: {
        enable: false,
        mode: 'bubble',
      },
      onclick: {
        enable: false,
        mode: 'push',
      },
      resize: true,
    },
    modes: {
      grab: {
        distance: 128,
        line_linked: {
          opacity: 1,
        },
      },
      bubble: {
        distance: 32,
        size: 64,
        duration: 2,
        opacity: 8,
        speed: 0.25,
      },
      repulse: {
        distance: 128,
        duration: 0.5,
      },
      push: {
        particles_nb: 4,
      },
      remove: {
        particles_nb: 2,
      },
    },
  },
  retina_detect: true,
};
