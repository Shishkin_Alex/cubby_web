/* eslint-disable global-require */
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueAuth from '@websanova/vue-auth';
import HighchartsVue from 'highcharts-vue';
import Highcharts from 'highcharts';
import App from './App.vue';
import store from './store';
import router from './router';
import './plugins/register-service-worker';
import './plugins/v-click-outside';
import './plugins/vue-ripple-directive';
import './plugins/components';
import '../node_modules/sl-vue-tree/dist/sl-vue-tree-minimal.css';
import { sync } from 'vuex-router-sync'

sync(store, router)

Vue.config.productionTip = false;
Vue.router = router;
Vue.use(HighchartsVue);
Highcharts.theme = {
  chart: {
    plotBackgroundColor: '#EBEBEB',
    style: {
      color: '#000000',
      fontFamily: 'Arial, sans-serif',
    },
  },
  colors: ['#595959', '#F8766D', '#A3A500', '#00BF7D', '#00B0F6', '#E76BF3'],
  xAxis: {
    labels: {
      style: {
        color: '#666666',
      },
    },
    title: {
      style: {
        color: '#000000',
      },
    },
    startOnTick: false,
    endOnTick: false,
    gridLineColor: '#FFFFFF',
    gridLineWidth: 1.5,
    tickWidth: 1.5,
    tickLength: 5,
    tickColor: '#666666',
    minorTickInterval: 0,
    minorGridLineColor: '#FFFFFF',
    minorGridLineWidth: 0.5,
  },
  yAxis: {
    labels: {
      style: {
        color: '#666666',
      },
    },
    title: {
      style: {
        color: '#000000',
      },
    },
    startOnTick: false,
    endOnTick: false,
    gridLineColor: '#FFFFFF',
    gridLineWidth: 1.5,
    tickWidth: 1.5,
    tickLength: 5,
    tickColor: '#666666',
    minorTickInterval: 0,
    minorGridLineColor: '#FFFFFF',
    minorGridLineWidth: 0.5,
  },
  legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
  background2: '#505053',
  dataLabelsColor: '#B0B0B3',
  textColor: '#C0C0C0',
  contrastTextColor: '#F0F0F3',
  maskColor: 'rgba(255,255,255,0.3)',
};
Highcharts.setOptions(Highcharts.theme);
Vue.use(VueAxios, axios);
const url = process.env.VUE_APP_API_URL;
Vue.apiUrl = url;
Vue.axios.defaults.baseURL = url;
Vue.use(VueAuth, {
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  rolesVar: 'role',
  fetchData: { url: '/current_user', method: 'GET', enabled: true },
  auth: {
    request(req, token) {
      // eslint-disable-next-line no-underscore-dangle
      this.options.http._setHeaders.call(this, req, { Authorization: `Bearer ${token}` });
    },
    response(res) {
      return (res.data || {}).token;
    },
  },
});

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
