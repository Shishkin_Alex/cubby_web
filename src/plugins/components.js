import Vue from 'vue';
import CButton from '@/components/CButton.vue';
import CInput from '@/components/CInput.vue';
import CSelect from '@/components/CSelect.vue';
import CSelectOption from '@/components/CSelectOption.vue';
import CModal from '@/components/CModal.vue';
import CTreeMenu from '@/components/CTreeMenu/index.vue';
import CPopover from '@/components/СPopover.vue';

Vue.component('c-button', CButton);
Vue.component('c-input', CInput);
Vue.component('c-select', CSelect);
Vue.component('c-select-option', CSelectOption);
Vue.component('c-modal', CModal);
Vue.component('c-tree-menu', CTreeMenu);
Vue.component('c-popover', CPopover);
