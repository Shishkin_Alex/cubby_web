import Vue from 'vue';
import VueRouter from 'vue-router';
import Tables from '../components/Tables/index.vue';
import Application from '../components/Application/index.vue';
import Rules from '../components/Rules/index.vue';
import Login from '../components/Login/index.vue';
import Integrator from '@/components/Integrator';
import CubeView from '@/components/CubeView';
import Process from '@/components/Integrator/Process';
import store from '@/store'
// import Dashboard from '../components/Dashboard/index.vue';

Vue.use(VueRouter);
const routes = [
  {
    path: '/',
    meta: {
      auth: true,
    },
  }, {
    path: '/integrator',
    component: Integrator,
    meta: {
      auth: true,
    },
    beforeEnter: (to, from, next) => {
      store.dispatch('integrator/refreshProcessList')
      next()
    }
  }, {
    path: '/cubeview',
    component: CubeView,
    meta: {
      auth: true,
    },
  },
  {
    path: '/process/:name',
    component: Process,
    meta: {
      auth: true,
    },
  },
  {
    path: '/rules/:cube',
    component: Rules,
    meta: {
      auth: true,
    },
  },
  {
    path: '/application', 
    component: Application,
    meta: {
      auth: true,
    },
  },
  // {
  //   path: '/dashboard/:name',
  //   component: Dashboard,
  //   meta: {
  //     auth: true,
  //   },
  // },
  { path: '/login', name: 'Login', component: Login },
];


export default new VueRouter({
  routes,
});
