export default {
  addCubeModal: false,
  addDimensionModal: false,
  addTableModal: false,
  dimensions: [],
  cubes: [],
  dimensionEditModal: {
    opened: false,
    dimension: '',
  },
  cubeRulesModal: {
    opened: false,
    cube: '',
  },
  table: {
    tabs: [],
    activeTab: null,
  },
  menu: [
    {
      name: 'Администрирование',
      admin: true,
      children: [
        {
          name: 'Сделать бэкап',
          children: [],
          mainClick: (store) => {
            store.dispatch('saveBackup');
          },
        }, {
          name: 'Кубы',
          children: [],
          actions: [{
            icon: 'add',
            func: (store) => {
              store.commit('openAddCubeModal');
            },
          }],
        }, {
          name: 'Измерения',
          children: [],
          actions: [{
            icon: 'add',
            func: (store) => {
              store.commit('openAddDimensionModal');
            },
          }],
        }, {
          name: 'Процессы',
          children: [],
          mainClick: (store, router) => {
            router.push('/integrator');
          },
        },
      ],
    }, {
      name: 'Приложения',
      children: [],
      actions: [{
        icon: 'add',
        func: (store) => {
          store.commit('openAddTableModal');
        },
      }],
    }, {
      name: 'Cubeview',
      mainClick: (store, router) => {
        router.push('/cubeview');
      },
      actions: [],
    }, {
      name: 'Дашборды',
      children: [{
        name: 'Аналитика',
        mainClick: (store, router) => {
          router.push('/dashboard/Аналитика');
        },
      }],
      actions: [],
    },
  ],
};
