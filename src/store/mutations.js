
export default {
  addTab: (state, name) => {
    if (!state.table.tabs.includes(name)) {
      state.table.tabs.push(name);
    }
    state.table.activeTab = name;
  },
  openCubeRulesModal: (state, payload) => {
    state.cubeRulesModal.opened = true;
    state.cubeRulesModal.cube = payload;
  },
  openDimensionEditModal: (state, payload) => {
    state.dimensionEditModal.opened = true;
    state.dimensionEditModal.dimension = payload;
  },
  closeDimensionEditModal: (state) => {
    state.dimensionEditModal.opened = false;
    state.dimensionEditModal.dimension = null;
  },
  closeCubeRulesModal: (state) => {
    state.cubeRulesModal.opened = false;
    state.cubeRulesModal.cube = '';
  },
  openAddTableModal: (state) => {
    state.addTableModal = true;
  },
  closeAddTableModal: (state) => {
    state.addTableModal = false;
  },
  openAddCubeModal: (state) => {
    state.addCubeModal = true;
  },
  openAddDimensionModal: (state) => {
    state.dimensionEditModal.opened = true;
    state.dimensionEditModal.dimension = null;
  },
  closeAddCubeModal: (state) => {
    state.addCubeModal = false;
  },
  closeAddDimensionModal: (state) => {
    state.addDimensionModal = false;
  },
  updForms: (state, payload) => {
    state.menu.forEach((v, i) => {
      if (v.name === 'Приложения' && payload) {
        state.menu[i].children = [...payload.map(f => ({
          name: f.Name,
          children: [],
          mainClick: (store, router) => {
            router.push("application", () => {})
            store.commit('application/selectTab', { name: f.Name, type: "form" });
          },
        }))];
      }
    });
  },
  updCubes: (state, payload) => {
    state.cubes = payload
    state.menu[0].children.forEach((v, i) => {
      if (v.name === 'Кубы' && payload) {
        state.menu[0].children[i].children = [...payload.map(c => ({
          name: c.Name,
          children: [...c.Dimensions ? c.Dimensions.map(d => ({ name: d, children: [] })) : []],
          actions: [{
            icon: 'notes',
            func: (store, name) => {
              store.commit('openCubeRulesModal', name);
            },
          }],
        }))];
      }
    });
  },
  updDimensions: (state, payload) => {
    state.dimensions = payload;
    state.menu[0].children.forEach((v, i) => {
      if (v.name === 'Измерения' && payload) {
        state.menu[0].children[i].children = [...payload.map(c => ({
          name: c.Name,
          children: [...c.Hierarchy.map(h => ({ name: h.Name, children: [] }))],
          actions: [{
            icon: 'edit',
            func: (store, name) => {
              store.commit('openDimensionEditModal', name);
            },
          }],
        }))];
      }
    });
  },
};
