import Vue from 'vue'
import {GetProcessListURL, AddProcessURL} from '@/utils/api'
import process from './process/index'

export default {
  namespaced: true,
  modules: {
    process
  },
  state: () => ({
    loading: {
      isProcessListLoading: true,
      isAddProcessLoading: false
    },
    processList: []
  }),
  getters: {},
  mutations: {
    setAddProcessLoading(state, payload) {
      state.loading.isAddProcessLoading = payload
    },
    setProcessListLoading(state, payload) {
      state.loading.isProcessListLoading = payload
    },
    updateProcessList(state, payload) {
      state.processList = payload
    }
  },
  actions: {
    async refreshProcessList({commit}) {
      commit('setProcessListLoading', true)
      Vue.axios
        .get(GetProcessListURL)
        .then(resp => {
          commit('updateProcessList', resp.data)
          commit('setProcessListLoading', false)
        })
    },
    async addProcess({commit, dispatch}, payload) {
      commit('setAddProcessLoading', true)
      Vue.axios
        .post(AddProcessURL, {name: payload})
        .then(() => {
          dispatch('refreshProcessList')
          commit('setAddProcessLoading', false)
        })
    }
  }
}
