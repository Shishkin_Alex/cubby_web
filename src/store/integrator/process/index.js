import Vue from 'vue'
import {GetProcessFilesURL, GetProcessFileURL} from '@/utils/api'

export default {
  namespaced: true,
  state: () => ({
    processFiles: [],
    processFile: null,
    loading: {
      isProcessFilesLoading: true,
      isProcessFileLoading: false
    }
  }),

  mutations: {
    setProcessFilesLoading(state, payload) {
      state.loading.isProcessFilesLoading = payload
    },
    setProcessFileLoading(state, payload) {
      state.loading.isProcessFileLoading = payload
    },
    updateProcessFiles(state, payload) {
      state.processFiles = payload
    },
    updateProcessFile(state, payload) {
      state.processFile = payload
    }
  },
  actions: {
    async getProcessFiles({rootState, commit}) {
      commit('setProcessFilesLoading', true)
      await Vue.axios
        .get(GetProcessFilesURL(rootState.route.params.name))
        .then(resp => {
          commit('updateProcessFiles', resp.data)
          commit('setProcessFilesLoading', false)
        })

    },
    async getProcessFile({commit, rootState}, payload) {
      commit('setProcessFileLoading', true)
      await Vue.axios
        .get(GetProcessFileURL(rootState.route.params.name, payload))
        .then(resp => {
          commit('updateProcessFile', resp.data)
          commit('setProcessFileLoading', false)
        })
    }
  }
}
