import Vue from 'vue'


export default {
  namespaced: true,

  state: () => ({
    activeTabIndex: 0,
    tabList: [],
  }),
  getters: {
    activeTab: state => {
      return state.activeTabIndex < state.tabList.length ? state.tabList[state.activeTabIndex] : null
    }
  },
  mutations: {
    closeTab(state, payload) {
      state.activeTabIndex = state.activeTabIndex > 0 ? state.activeTabIndex - 1 : 0
      state.tabList = state.tabList.filter(tab => tab.name != payload)
    },
    selectTab(state, payload) {
      if (state.tabList.filter((t) => t.name == payload.name).length > 0 ) {
        state.tabList.forEach((e, i ) => {
          if (e.name == payload.name) {
            state.activeTabIndex = i
          }
        })
      } else {
        state.tabList.push(payload)
        state.activeTabIndex = state.tabList.length - 1
      }
    }
  },
  actions: {
    
  }
}
