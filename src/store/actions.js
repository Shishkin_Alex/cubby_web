import Vue from 'vue';

const apiUrl = process.env.VUE_APP_API_URL;

export default {
  saveBackup: () => {
    Vue.axios.get(`${apiUrl}/olap/save`);
  },
  addForm: ({ dispatch, commit }, payload) => {
    Vue.axios.post(`${apiUrl}/forms`, payload).then(() => {

      dispatch('getForms');
    });
  },
  getForms: ({ commit }) => {
    // когда-нибудь я пойму почему так, но не сегодня
    Vue.axios.get(`${apiUrl}/forms`).then((resp) => {
      commit('updForms', resp.data);
    });
  },
  getCubes: ({ commit }) => {
    Vue.axios.get(`${apiUrl}/olap/cubes`).then((resp) => {
      commit('updCubes', resp.data);
    });
  },
  getDimensions: ({ commit }) => {
    Vue.axios.get(`${apiUrl}/olap/dimensions`).then((resp) => {
      commit('updDimensions', resp.data);
    });
  },
};
