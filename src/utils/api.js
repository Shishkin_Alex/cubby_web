

const integratorGroup = '/integrator'
export const GetProcessListURL = integratorGroup + '/'
export const AddProcessURL = integratorGroup + '/'
export const GetProcessFilesURL = (processName) => `${integratorGroup}/${processName}/files`
export const GetProcessFileURL = (processName, filename) => `${integratorGroup}/${processName}/file/${filename}`

export default ''
